# Shell (bash) script to issue OpenID Authorization Requests, and exchange received Authorization Codes as well as Access Tokens.

Currently Authentication and Consenting steps are tailored towards UI of [DenicID](https://www.denic.de/service/denic-id/).
All other steps should work with other OpenID endpoints, too.

Script emits usage info upon:

```
$ ./id4me-authz-workflow.sh --help
```
