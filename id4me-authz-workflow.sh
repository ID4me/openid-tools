#! /bin/bash


id4me=
password=

clientID=
clientRedirectURI=
clientSecret=

acceptLanguage=en
targetURI=https://id.staging.denic.de

claimsJsonFile=claims.json
openIdScope=openid

state=$(cat /dev/urandom | tr --delete --complement 'a-zA-Z0-9' | fold --width 32 | head --lines 1)


if [ -f .envvar ]; then
	echo "Loading parameters from file '.envvar'."
	source .envvar;
fi


function usage {
	usage="Usage: ./$(basename "$0")
	Preloads values from file '.envvar' (if available);
	the following params overwrite them:
	-i/--id4me       : ID4me
	-p/--password    : ID4me password
	-c/--client-id   : Client ID
	-s/--secret      : Client secret
	-r/--redirect-uri: Client Redirect URI
	[-o/--scopes     : Further OpenID Scopes to sent (space-separated, beside required default '$openIdScope')]
	[-l/--language   : Language HTTP request header (default '$acceptLanguage')]
	[-t/--target-uri : Target URI (default '$targetURI')]
	[-m/--claims     : File to read claims from (default '$claimsJsonFile')]
	[-x/--no-claims  : No claims are sent (overwrites '-m' param value)]
	[-h/--help]"
	echo "$usage"
}

# Solution taken from here: https://stackoverflow.com/questions/6250698/how-to-decode-url-encoded-string-in-shell
function urlDecode { : "${*//+/ }"; echo -e "${_//%/\\x}"; }

function echoToSysErr {
	1>&2 echo "$1"	
}

function prettyPrintBase64DecodedJSON {
	local base64Value="$1"
	local base64ValueLength=$(expr length "${base64Value}")
	while [ $[base64ValueLength % 4] -ne 0 ]; do
		base64Value+='='
		base64ValueLength=$(expr length "${base64Value}")
	done
	echo "$base64Value" | base64 -d | jq .
}

function grepForActionOfFormIn {
	local fileToParse="$1"
	grep '<form' "$fileToParse" \
		| grep --perl-regexp \
			--only-matching \
			'action="\K([^"]+)'
	# Above grep idea taken from here: https://unix.stackexchange.com/questions/371664/shell-test-whether-multiple-lines-string-contains-specified-pattern-in-last-line
}

function grepForValueOfInputIn {
	local fileToParse="$1"
	local nameOfInputElement="$2"
	grep '<input' "$fileToParse" \
		| grep "name=\"${nameOfInputElement}\"" \
			| grep --perl-regexp \
				--only-matching \
				'value="\K([^"]+)(?=")'
	# Above grep idea taken from here: https://stackoverflow.com/questions/18892670/can-not-extract-the-capture-group-with-neither-sed-nor-grep/18892742
}

function handleTwoFactorAuthentication {
	echo
	echo "Two Factor Authentication REQUIRED!"
	local fileToParse="$1"
	local passwordOK=$(grepForValueOfInputIn "$fileToParse" 'passwordOK')
	read -t 180 -p "Please enter your 2FA verification code (within 3 minutes): " verificationCode
	echo
	echo Issuing Two-Factor Authentication Request:
	echo ==========================================
	twoFAResponseTargetFile=2fa-response.html
	echo "Writing response to '$twoFAResponseTargetFile' ..."
	curl --request POST \
		--cookie-jar openid-authz.cookie \
		--data-urlencode "sessionID=${sessionID}" \
		--data-urlencode "domainID=${id4me}" \
		--data-urlencode "passwordOK=${passwordOK}" \
		--data-urlencode "verificationCode=${verificationCode}" \
		--header 'Content-Type: application/x-www-form-urlencoded' \
		--header "Accept-Language: $acceptLanguage" \
		--silent \
		--output $twoFAResponseTargetFile \
		--dump-header ${twoFAResponseTargetFile}.header \
		${targetURI}/authenticate/totp
	dos2unix --quiet ${twoFAResponseTargetFile}.header
}

function checkForResponseCode {
	local responseBodyFile="$1"
	local responseHeaderFile="$responseBodyFile.header"
	local expectedCode="$2"
	local receivedCode=$(grep --perl-regexp --only-matching 'HTTP/[\d\.]+\s+\K(\d+)' "$responseHeaderFile")
	if [ ! "$expectedCode" = "$receivedCode" ]; then
		echo "Received UNEXPECTED HTTP response code: '$receivedCode' (instead of '$expectedCode')!"
		responseContentType=$(grep --perl-regexp --only-matching 'content-type:\s+\K(.+)' "${responseHeaderFile}")
		if [ "$responseContentType" = "application/json" ]; then
			echo "Response body (to tackle down problem's cause):"
			echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
			cat "$loginResponseTargetFile" | jq .
			echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		else
			echo "You may find further information by checking:"
			echo "- HTTP response body file '$responseBodyFile' and/or"
			echo "- HTTP response header file '$responseHeaderFile'"
		fi
		echo "EXITING!"; exit 50
	fi
}

function queryTokenEndpointOf {
	local target="$1"
	local result=$(curl --silent \
		"${target}/.well-known/openid-configuration" \
		| jq --raw-output \
			.token_endpoint)
	if [ "$result" = "" ]; then
		echoToSysErr "Could NOT got Token Endpoint URL by querying '${target}/.well-known/openid-configuration'!"
		echoToSysErr "EXITING!"; exit 11
	fi
	echo "$result"
}

function queryUserInfoEndpointOf {
	local target="$1"
	local result=$(curl --silent \
		"${target}/.well-known/openid-configuration" \
		| jq --raw-output \
			.userinfo_endpoint)
	if [ "$result" = "" ]; then
		echoToSysErr "Could NOT got Token Endpoint URL by querying '${target}/.well-known/openid-configuration'!"
		echoToSysErr "EXITING!"; exit 11
	fi
	echo "$result"
}

function getTokenIssuerFrom {
	local token="$1"
	local result=$(echo "$token" \
		| jq --raw-output \
			.iss)
	if [ "$result" = "" ]; then
		echoToSysErr "Could NOT extract 'iss' value from Token '${token}'!"
		echoToSysErr "EXITING!"; exit 60
	fi
	echo "$result"
}

function jwtPayloadOf {
	local SavedIFS="$IFS"
	IFS='.'
	read -ra elementsOfJWT <<< "$1"
	IFS="$SavedIFS"
	echo "${elementsOfJWT[1]}"
}


# CLI param processing:
while [ "$1" != "" ]; do
    case $1 in
        -c | --client-id )      shift
                                clientID=$1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        -i | --id4me )          shift
                                id4me=$1
                                ;;
        -l | --language )       shift
                                acceptLanguage=$1
                                ;;
        -m | --claims )         shift
                                claimsJsonFile=$1
                                ;;
        -o | --scopes )         shift
                                furtherScopes=$1
                                ;;
        -p | --password )       shift
                                password=$1
                                ;;
        -r | --redirect-uri )   shift
                                clientRedirectURI=$1
                                ;;
        -s | --secret )         shift
                                clientSecret=$1
                                ;;
        -t | --target-uri )     shift
                                targetURI=$1
                                ;;
        -x | --no-claims )      claimsJsonFile=""
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

# CLI param checks:
if [ "$clientID" = "" ]; then
	echo "Missing Client ID (parameter -c)!"
	exit 2
fi

if [ "$clientSecret" = "" ]; then
	echo "Missing Client Secret (parameter -s)!"
	exit 2
fi

if [ "$id4me" = "" ]; then
	echo "Missing ID4me (parameter -i)!"
	exit 2
fi

if [ "$password" = "" ]; then
	echo "Missing ID4me Password (parameter -p)!"
	exit 2
fi

if [ "$clientRedirectURI" = "" ]; then
	echo "Missing Client Redirect URI (parameter -r)!"
	exit 2
fi

if [ "$claimsJsonFile" != "" ]; then
	if [ ! -f $claimsJsonFile ]; then
		echo "Missing Claims JSON file '$claimsJsonFile' (set via --claims parameter)!"
		exit 2
	fi
fi


echo
echo Using following param values:
echo =============================
echo "id4me            : '$id4me'"
#echo "password         : '$password'"
echo "language         : '$acceptLanguage'"
echo "clientID         : '$clientID'"
echo "clientRedirectURI: '$clientRedirectURI'"
#echo "clientSecret     : '$clientSecret'"
echo "targetURI        : '$targetURI'"
echo "furtherScopes    : '$furtherScopes'"


	echo
if [ "$claimsJsonFile" = "" ]; then
	claimsKeyAndValueToSend=""
	echo "Sending NO Claims JSON!"
else
	plainClaimsJSON=$(cat $claimsJsonFile)
	compactClaimsJSON=$(echo "$plainClaimsJSON" | jq --compact-output .)
	jqResult="$?"
	if [ "$jqResult" = "0" ]; then
		claimsJSONToSend="$compactClaimsJSON"
		echo "The following Claims JSON is sent (formatted for display):"
		echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
		echo "$claimsJSONToSend" | jq .
		echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	else
		echo "CAUTION: Content of file '$claimsJsonFile' seems to NOT contain well-formed JSON!"
		echo "Usually, you will find a more detailed error message from the parser two lines above."
		claimsJSONToSend="$plainClaimsJSON"
		echo "I'm going to sent this Claims value, anyway:"
		echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
		echo "$claimsJSONToSend"
		echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	fi
	claimsKeyAndValueToSend="claims=$claimsJSONToSend"
fi

scopesToSend="${openIdScope}"
if [ "$furtherScopes" != "" ]; then
	scopesToSend="${scopesToSend} ${furtherScopes}"
fi


echo
echo Issuing OpenID Authorization Request:
echo =====================================
loginResponseTargetFile=login-response.html
echo "Writing response to '$loginResponseTargetFile' ..."
curl --get \
	--silent \
	--data 'response_type=code' \
	--data-urlencode "scope=${scopesToSend}" \
	--data-urlencode "client_id=${clientID}" \
	--data-urlencode "state=${state}" \
	--data-urlencode "redirect_uri=${clientRedirectURI}" \
	--data-urlencode "login_hint=${id4me}" \
	--data-urlencode "${claimsKeyAndValueToSend}" \
	--header "Accept-Language: $acceptLanguage" \
	--output $loginResponseTargetFile \
	--dump-header ${loginResponseTargetFile}.header \
	"${targetURI}/login"

dos2unix --quiet ${loginResponseTargetFile}.header
checkForResponseCode "$loginResponseTargetFile" '200'

sessionID=$(grepForValueOfInputIn "$loginResponseTargetFile" "sessionID")
if [ "$sessionID" = "" ]; then
	echo "Received NO Session ID from Authorisation Endpoint!"
	responseContentType=$(grep --perl-regexp --only-matching 'content-type: \K(.+)' "${loginResponseTargetFile}.header")
	if [ "$responseContentType" = "application/json" ]; then
		echo "Response body (to tackle down problem's cause):"
		echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
		cat "$loginResponseTargetFile" | jq .
		echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	fi
	echo "EXITING!"; exit 12
fi

echo "... received Authorisation Endpoint's session ID: '$sessionID'"

authenticateUriPath=$(grepForActionOfFormIn "$loginResponseTargetFile")
if [[ ! $authenticateUriPath =~ .?authenticate ]]; then
	echo
	echo "Something went WRONG: Received unexpected HTML form's action value '$authenticateUriPath'!"
	echo 'EXITING!'; exit 40
fi


echo
echo Issuing Authentication request:
echo ===============================
authenticateResponseTargetFile=authenticate-response.html
echo "Writing response to '$authenticateResponseTargetFile' ..."
curl --request POST \
	--cookie-jar openid-authz.cookie \
	--data-urlencode "sessionID=${sessionID}" \
	--data-urlencode "domainID=${id4me}" \
	--data-urlencode "password=${password}" \
	--header 'Content-Type: application/x-www-form-urlencoded' \
	--header "Accept-Language: $acceptLanguage" \
	--silent \
	--output $authenticateResponseTargetFile \
	--dump-header ${authenticateResponseTargetFile}.header \
	${targetURI}/${authenticateUriPath}

dos2unix --quiet ${authenticateResponseTargetFile}.header
checkForResponseCode "$authenticateResponseTargetFile" '200'

consentUriPath=$(grepForActionOfFormIn "$authenticateResponseTargetFile")
if [[ $consentUriPath =~ .?authenticate/totp ]]; then
	handleTwoFactorAuthentication "$authenticateResponseTargetFile"
	authenticateResponseTargetFile="$twoFAResponseTargetFile"
elif [[ ! $consentUriPath =~ .?consent ]]; then
	echo
	echo "Something went WRONG: Received unexpected HTML form's action value '$consentUriPath'!"
	echo '- Maybe mismatching password credential?'
	echo 'EXITING!'; exit 41
fi


# Extracting Client's name works in a simple way only with English version of UI!
#clientName=$(grep --perl-regexp --only-matching 'at <.+>\K([^<]+)(?=<)' $authenticateResponseTargetFile)
#echo "... received name of Client/Relying Party: '${clientName}'"
suggestedClaims=$(grepForValueOfInputIn "$authenticateResponseTargetFile" "suggestedClaims")
urlDecodedSuggestedClaims=$(urlDecode $suggestedClaims)
echo "... received list of Claims to consent: '${urlDecodedSuggestedClaims}'"


echo
echo Issuing Consenting request:
echo ===========================
echo "Consenting all suggested claims: '${urlDecodedSuggestedClaims}'"
consentResponseTargetFile=consent-response.html
echo "Writing response to '$consentResponseTargetFile' ..."
allClaimsToPostAsData=$(echo -n $suggestedClaims | sed 's/,/\&claim=/g')
curl --request POST \
	--cookie openid-authz.cookie \
	--data-urlencode "sessionID=${sessionID}" \
	--data-urlencode "suggestedClaims=${urlDecodedSuggestedClaims}" \
	--data "claim=${allClaimsToPostAsData}" \
	--data 'consenting=true' \
	--header 'Content-Type: application/x-www-form-urlencoded' \
	--header "Accept-Language: $acceptLanguage" \
	--silent \
	--output $consentResponseTargetFile \
	--dump-header ${consentResponseTargetFile}.header \
	${targetURI}/consent

dos2unix --quiet ${consentResponseTargetFile}.header
checkForResponseCode "$consentResponseTargetFile" '303'
echo "... FINISHED OpenID Authorisation Worfklow!"

echo
echo Data from response redirecting to the client/relying party:
echo -----------------------------------------------------------
locationHeader=$(grep --perl-regexp --only-matching 'location: \K(.+)' ${consentResponseTargetFile}.header)
echo "Location header (to get redirected to): '$locationHeader'"
echo
stateResponse=$(echo -n "$locationHeader" | grep --perl-regexp --only-matching 'state=\K([^&]+)')
if [ $stateResponse != $state ]; then
	echo "Sent state '$state' with OpenID Authorisation Request, but now received MISMATCHING state '$stateResponse'!"
	echo "EXITING!"; exit 13
fi

error=$(echo -n "$locationHeader" | grep --perl-regexp --only-matching 'error=\K([^&]+)')
if [ "$error" != "" ]; then
	echo "OpenID workflow FAILED, server redirects to Client/Relying Party with error '$error'!"
	errorDescription=$(echo -n "$locationHeader" | grep --perl-regexp --only-matching 'error_description=\K([^&]+)')
	if [ "$errorDescription" != "" ]; then
		echo "Server's error description: '$(urlDecode "$errorDescription")'."
	fi
	echo "EXITING!"; exit 30
fi

authorisationCode=$(echo -n "$locationHeader" | grep --perl-regexp --only-matching 'code=\K([^&]+)')
if [ "$authorisationCode" = "" ]; then
	echo "Have NOT got an OpenID Authz Code!"
	echo "EXITING!"; exit 10
fi

echo "OpenID Authorization Code (to exchange at Token Endpoint): '$authorisationCode'"


tokenEndpointURL=$(queryTokenEndpointOf "${targetURI}")
echo
clientBasicAuthz=$(echo -n "${clientID}:${clientSecret}" | base64)
echo "Exchanging AUTHORISATION CODE on Token Endpoint '$tokenEndpointURL':"
echo ======================================================================
tokenResponseTargetFile=token-response.json
echo "Writing response to '$tokenResponseTargetFile' ..."
curl --request POST \
	--data 'grant_type=authorization_code' \
	--data-urlencode "code=${authorisationCode}" \
	--data-urlencode "redirect_uri=${clientRedirectURI}" \
	--header 'Content-Type: application/x-www-form-urlencoded' \
	--header "Authorization: Basic ${clientBasicAuthz}" \
	--silent \
	--output $tokenResponseTargetFile \
	--dump-header ${tokenResponseTargetFile}.header \
	$tokenEndpointURL

dos2unix --quiet "${tokenResponseTargetFile}.header"
checkForResponseCode "$tokenResponseTargetFile" '200'

echo ... received Token Endpoint response:
echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
cat "$tokenResponseTargetFile" | jq .
echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^


tokenType=$(jq --raw-output .token_type ${tokenResponseTargetFile})
idTokenJWT=$(jq --raw-output .id_token ${tokenResponseTargetFile})
accessTokenJWT=$(jq --raw-output .access_token ${tokenResponseTargetFile})

echo
if [ "$idTokenJWT" = "" ]; then
	echo "Received NO ID Token!"
else
	payloadOfIdToken=$(jwtPayloadOf "$idTokenJWT")
	echo Base64-decoded ID Token payload:
	echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
	prettyPrintBase64DecodedJSON "$payloadOfIdToken"
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
fi

echo
if [ "$accessTokenJWT" = "" ]; then
	echo "Received NO Access Token!"
	echo "EXITING!"; exit 20
else
	payloadOfAccessToken=$(jwtPayloadOf "$accessTokenJWT")
	accessTokenJSONPayload=$(prettyPrintBase64DecodedJSON "$payloadOfAccessToken")
	echo Base64-decoded Access Token payload:
	echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
	echo "$accessTokenJSONPayload"
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	accessTokenIssuer=$(getTokenIssuerFrom "$accessTokenJSONPayload")
fi


echo
echo "Access Token Issuer: '$accessTokenIssuer'"
userInfoEndpoint=$(queryUserInfoEndpointOf "$accessTokenIssuer")
echo
echo "Exchanging ACCESS TOKEN at issuer's User Info Endpoint '$userInfoEndpoint':"
echo ================================================================================
userInfoResponseTargetFile=user-info-response.json
echo "Writing response to '$userInfoResponseTargetFile' ..."
curl --request POST \
	--silent \
	--header "Authorization: Bearer ${accessTokenJWT}" \
	--output "$userInfoResponseTargetFile" \
	--dump-header "${userInfoResponseTargetFile}.header" \
	"$userInfoEndpoint"

dos2unix --quiet "${userInfoResponseTargetFile}.header"
checkForResponseCode "$userInfoResponseTargetFile" '200'

echo ... received user info response:
echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
cat "$userInfoResponseTargetFile" | jq .
echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^


distributedClaimsEndpoints=$(cat "$userInfoResponseTargetFile" | jq -r ._claim_sources[]?.endpoint)
COUNTER=1
for distributedClaimsEndpoint in $distributedClaimsEndpoints; do
	echo
	echo "Exchanging ACCESS TOKEN at ${COUNTER}. Distributed Claims' User Info Endpoint '$distributedClaimsEndpoint':"
	echo ================================================================================
	distributedClaimsResponseTargetFile="distributed-claims-response-$COUNTER.json"
	echo "Writing response to '$distributedClaimsResponseTargetFile' ..."
	curl --request GET \
		--silent \
		--header "Authorization: Bearer ${accessTokenJWT}" \
		--output "$distributedClaimsResponseTargetFile" \
		--dump-header "${distributedClaimsResponseTargetFile}.header" \
		"$distributedClaimsEndpoint"

	dos2unix --quiet "${distributedClaimsResponseTargetFile}.header"
	checkForResponseCode "$distributedClaimsResponseTargetFile" '200'

	distributedClaimsJWTPayload="$(cat $distributedClaimsResponseTargetFile | jq .)"
	echo ... received user info response:
	echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
	echo "$distributedClaimsJWTPayload"
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	let COUNTER+=1
done


#tokenMicroServiceURL=https://id.staging.denic.de/accesstoken
#echo
#echo "Exchanging ACCESS TOKEN at Access Token Microservice '${tokenMicroServiceURL}':"
#echo ================================================================================
#userInfoResponseTargetFile=user-info-response.json
#echo "Writing response to '$userInfoResponseTargetFile' ..."
#curl --request GET \
#	--silent \
#	--output "$userInfoResponseTargetFile" \
#	--dump-header "${userInfoResponseTargetFile}.header" \
#	"${tokenMicroServiceURL}/${accessTokenJWT}"
#
#dos2unix --quiet "${userInfoResponseTargetFile}.header"
#checkForResponseCode "$userInfoResponseTargetFile" '200'
#
#echo ... received user info response:
#echo vvvvvvvvvvvvvvvvvvvvvvvvvvvv
#cat "$userInfoResponseTargetFile" | jq .
#echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^
